'''Created by Nandagopal (Azeyru)'''
import os
import os.path
import csv as csv
import numpy as np
import pandas as pd
from pylab import * 
import matplotlib.pyplot as plt
from math import sqrt

class KnnDataSet:

	def __init__(self, data_list):
		self.s_x = data_list[0]
		self.s_y = data_list[1]
		self.p_x = data_list[2]
		self.p_y = data_list[3]
		self.label = data_list[4]
		
	def getSepalLength(self):
		return self.s_x
		
	def getSepalWidth(self):
		return self.s_y
	
	def getPetalLenght(self):
		return self.p_x
		
	def getPetalWidth(self):
		return self.p_y
		
	def getLable(self):
		return self.label

class Readdata:

	def read_csv(self):
		
		current_dir_path = os.path.dirname(os.path.realpath(__file__))
		get_csv_file = current_dir_path + "\\Iris.csv"
		
		#slef.data = np.loadtxt(get_csv_file, delimiter=',', skiprows=1, usecols=range(2,6))
		self.data = pd.read_csv(get_csv_file, sep=",")
		dat = self.data
		dat.drop('Id', axis=1, inplace=True)
		
		self.rows = dat.shape[0]
		self.columns = dat.shape[1]
		
	def split_data(self):
		#list = [[] for i in range(self.columns)]
		self.data_list = []
		itme_count=0

		for lines in self.data:
			if not (isinstance(lines, float)):
				#list[itme_count].append(dat[lines].values)
				self.data_list.append(self.data[lines].values)
			itme_count += 1

		return self.data_list, self.rows
		
class KNN:

	print("K-NN Algorithm")
	
	def __init__(self):

		data_Knn = Readdata()
		data_Knn.read_csv()
		self.data_set, self.count = data_Knn.split_data()
		
	def predict_knn(dataset, k, labl):
		
		npdataSet = np.array(dataset)
		indx = np.argpartition(npdataSet, k)
		element_list = npdataSet[indx[:k]]
		predicted = []
		for elements in element_list:
			predicted.append(npdataSet.tolist().index(elements))
			
		predicted_list = labl[predicted]
		
		print(predicted_list)
		#print(element_list)
		
		element = max(set(predicted_list), key = predicted_list.tolist().count)
		
		print("PREDICTED: " + element)
		
	def get_calc(self, x, y, a, b, k):
		ds = KnnDataSet(self.data_set)
		
		self.s_x = ds.getSepalLength()
		self.s_y = ds.getSepalWidth()
		self.p_x = ds.getPetalLenght()
		self.p_y = ds.getPetalWidth()
		self.labl = ds.getLable()
		
		self.x = x
		self.y = y
		self.a = a
		self.b = b
		
		sq = []
		
		for i in range(self.count):
			sq = KNN.get_euclidean_distance(self)
		
		KNN.predict_knn(sq, k, self.labl)
	
	def get_euclidean_distance(self):
		sum = (((self.x - self.s_x)**2) + ((self.y - self.s_y)**2) + ((self.a - self.p_x)**2) + ((self.b - self.p_y)**2))
		
		euc_dst = []
		for values in sum:
			euc_dst.append(sqrt(values))
		
		return euc_dst
		
def predict(s_lenght, s_width, p_length, p_width, k):

	knn = KNN()	
	
	print("for k = {} and \nData: {}, {}, {}, {}".format(k, s_lenght, s_width, p_length, p_width))
	
	knn.get_calc(s_lenght, s_width, p_length, p_width, k)
		
def main():
	print("From Iris Dataset")
	#predict(4.5, 3.1, 1.5, 0.4, 25)
	predict(8.0, 3.3, 4.1, 0.2, 3)
	
if __name__ == '__main__':
	main()
