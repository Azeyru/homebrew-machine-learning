'''Created by Nandagopal (Azeyru)'''
import os
import os.path
import glob
import subprocess
import numpy as np
import csv as csv
from pylab import *
import matplotlib.pyplot as plt
from math import sqrt

x_data = []
y_data = []

def predict(y_intercept, slope, sample_ip):
	predicted = float(y_intercept) + (slope * sample_ip)
	
	return predicted

def get_y_intercept(slope, x_mean, y_mean):
	yintercept = y_mean - (slope * x_mean)
	
	return yintercept

def get_slope(covar, sum_x, sum_y, count):
	
	std_deviation_x = sqrt( sum_x/(count-1))
	std_deviation_y = sqrt( sum_y/(count-1))
	
	slope = covar * (std_deviation_y / std_deviation_x)
	
	print("slope: " + str(slope))
	return slope

def get_coverience(mean_area, mean_amount, count):
	x_error = []
	x_error_sq = []
	y_error = []
	y_error_sq = []
	
	for data1 in x_data:
		#print("X_ERROR: " + str(data1 - mean_area))
		x_error.append(data1 - mean_area)
		x_error_sq.append((data1 - mean_area)**2)
		#print("X_ERROR: " + str((data1 - mean_area)**2))
	
	for data2 in y_data:
		#print("Y_ERROR: " + str(data2 - mean_amount))
		y_error.append(data2 - mean_amount)
		y_error_sq.append((data2 - mean_amount)**2)
		#print("Y_ERROR: " + str((data2 - mean_amount)**2))
	
	upper_half = []
	for i in range(count-1):
		ele = x_error[i] * y_error[i]
		upper_half.append(ele)
	
	sum_x_error_sq = sum(x_error_sq)
	sum_y_error_sq = sum(y_error_sq)
	
	r_upper = sum(upper_half)
	r_lower = sqrt(sum_x_error_sq * sum_y_error_sq)

	cp_covar = (r_upper/r_lower)
	print("COVAR........."+str(cp_covar))
	return cp_covar, sum_x_error_sq, sum_y_error_sq
	
	
def show_plot(x, y):

	fit = np.polyfit(x,y,1)
	plotfn = np.poly1d(fit) 
	
	plt.plot(x, y, 'yo', x, plotfn(x), '--k')
	#plt.xlim(0, 1600)
	plt.xlabel('area (sq)')
	
	#plt.ylim(0, 30000)
	plt.ylabel('rate (inr)')
	
	plt.savefig("Linear Reg Plot.png")
	#plt.show()
	
	#x = np.array(x)
	#y = np.array(y)
	#m, b = polyfit(x, y, 1)
	#plot(x, y, 'yo', x, m*x+b, '--k') 
	#plt.show()

def read_csv():
	current_dir_path = os.path.dirname(os.path.realpath(__file__))
	get_csv_file = current_dir_path + "\\data.csv"
	
	count_of_csv_data = 0
	dataset = []
	
	with open(get_csv_file, "r") as data_from_csv:
		next(data_from_csv)
		csv_reader = csv.reader(data_from_csv, delimiter=',')
		for row in csv_reader:
			if not row:
				continue
			dataset.append(row)
			count_of_csv_data += 1
	
	return dataset, count_of_csv_data

def get_mean(csv_datas, count_of_csv_data):
	print("Calculating Mean from DATA_CSV: ")
	total_area = 0
	total_amount = 0
	
	print("Data from CSV: ")
	for lines in csv_datas:
		print(str(lines))
		total_area = total_area + float(str(lines[0]))
		x_data.append(float(str(lines[0])))
		total_amount = total_amount + float(str(lines[1]))
		y_data.append(float(str(lines[1])))
	
	mean_area = total_area/count_of_csv_data
	mean_amount = total_amount/count_of_csv_data
	
	show_plot(x_data, y_data)
	
	return mean_area, mean_amount

def main():
	
	print("Read Sample data from CSV ")
	csv_datas, count_of_csv_data = read_csv()
	
	mean_area, mean_amount = get_mean(csv_datas, count_of_csv_data)
	
	print("Mean Area: " + str(mean_area))
	print("Mean Amount: " + str(mean_amount))
	
	sample_ip = 1500
	
	co_var, sum_x_sqr, sum_y_sqr = get_coverience(mean_area, mean_amount, count_of_csv_data)
	
	slope = get_slope(co_var, sum_x_sqr, sum_y_sqr, count_of_csv_data)
	
	y_intercept = get_y_intercept(slope, mean_area, mean_amount)
	
	print("Slope of the cuttent data: " + str(slope))
	print("Y Intercept: " + str(y_intercept))
	
	predict_y = predict(y_intercept, slope, sample_ip)
	
	print("The predicted output for the input {} is {}".format(str(sample_ip), str(predict_y)))
	

if __name__ == '__main__':
	print("Demo program on Simple Linear Regression")
	main()
